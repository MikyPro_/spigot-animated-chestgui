package it.mikypro.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public final class GuiPlugin extends JavaPlugin implements CommandExecutor {

    private Inventory guiInventory = Bukkit.createInventory(null, 9, "§bMikyPro_'s gui");

    @Override
    public void onEnable() {
        // Plugin startup logic

        getCommand("gui").setExecutor(this);

        ItemStack stack = new ItemStack(Material.BOW, 1);
        ItemMeta meta = stack.getItemMeta();

        new BukkitRunnable() {
            int i = 0;
            @Override
            public void run() {
                i += 1;
                meta.setDisplayName("§a" + i);
                stack.setItemMeta(meta);
                guiInventory.setItem(8, stack);
            }
        }.runTaskTimer(this, 0, 20);

        for (int i=0; i<=5; i++)
            guiInventory.setItem(i, new ItemStack(Material.ANVIL, 1));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!cmd.getName().equalsIgnoreCase("gui")) return false;
        if (!(sender instanceof Player)) return false;

        ((Player)sender).openInventory(guiInventory);
        return true;
    }
}
